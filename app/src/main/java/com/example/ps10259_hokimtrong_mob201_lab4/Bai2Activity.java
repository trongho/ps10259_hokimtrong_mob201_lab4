package com.example.ps10259_hokimtrong_mob201_lab4;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class Bai2Activity extends AppCompatActivity {
    Button btnCheckConection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bai2);

        btnCheckConection = findViewById(R.id.btnCheckConection);

        btnCheckConection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager connectivitymanager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo[] networkInfo = connectivitymanager.getAllNetworkInfo();

                for (NetworkInfo netInfo : networkInfo) {

                    if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))

                        if (netInfo.isConnected())

                            Toast.makeText(Bai2Activity.this,"Connected to WiFi",Toast.LENGTH_LONG).show();

                    if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))

                        if (netInfo.isConnected())

                            Toast.makeText(Bai2Activity.this,"Connected to Mobile Data",Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
